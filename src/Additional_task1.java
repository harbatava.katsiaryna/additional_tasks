import java.util.Scanner;

public class Additional_task1 {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        float x = scanner.nextInt();
        float y = scanner.nextInt();

        float sum = x + y;
        float difference = x - y;
        float product = x * y;
        float quotient = x / y;

        System.out.println("Sum: " + sum);
        System.out.println("Difference: " + difference);
        System.out.println("Product: " + product);
        System.out.println("Quotient: " + quotient);
    }
}
